from ruminate.web import app
import os
debug = os.environ.get("RUMINATE_DEBUG", False)
port = int(os.environ.get('PORT', 5000))
app.run(host='0.0.0.0',port=port,debug=debug)
