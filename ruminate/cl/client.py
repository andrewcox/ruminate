from ruminate.data.access import DataAccess
from ruminate.data.load import GoogleImport, FeedEater
import sys
import traceback

def add_user(username,password):
    da = DataAccess()
    try:
        user = da.add_user(username,password)
        if user:
            print "user created"
            exit(0)
        else:
            print "user not created"
            exit(3)
    except Exception as e:
        print "user not created"
        print e
        exit(3)

def import_google_xml(username,filepath):
    gi = GoogleImport()
    try:
        #TODO: Check for user here
        data = gi.import_from_file(filepath)
        print "found %s feeds" % len(data["feeds"])
        print "found %s groups" % len(data["groups"])
        da = DataAccess()
        da.add_feeds(data["feeds"])
        da.add_groups(username, data["groups"])
    except Exception as e:
        print "import not successful"
        print e
        exit(3)

def load_feeds():
    fe = FeedEater()
    da = DataAccess()
    try:
        feeds = da.get_feeds(sort=[("last_updated",1)])
        for feed in feeds:
            print feed["xmlUrl"]
            state = "success"
            entries = []
            try:
                entries = fe.eat(feed)
            except Exception as e:
                state = "fail"
                print "Eating failed"
                print e

            try:
                if entries:
                    da.add_entries(entries)
                    print "added %s entries to %s" % (len(entries),feed["title"])
            except Exception as e:
                state = "fail"
                print "adding entries failed"
                print e

            feed["last_updated_status"] = state
            da.save_feed(feed)
            
    except Exception as e:
        print "loading not completed"
        print e
        exit(3)
        


def main(cl_args):
    #TODO: move to the right module
    if len(cl_args) == 0:
        print "No command specified"
        exit(1)
    if cl_args[0] == "add_user":
        if len(cl_args) != 3:
            print "useage: add_user username password"
            exit(2)
        add_user(cl_args[1],cl_args[2])
    elif cl_args[0] == "import_google_xml":
        if len(cl_args) != 3:
            print "useage: import_google_xml username filepath"
            exit(2)
        import_google_xml(cl_args[1],cl_args[2])
    elif cl_args[0] == "load_feeds":
        load_feeds()
    else:
        print "unknown command"
        exit(2)

    

