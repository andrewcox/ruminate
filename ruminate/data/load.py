from xml.dom.minidom import parse
import datetime
import hashlib
import os
import urllib2
import feedparser

class GoogleImport(object):

    def import_from_file(self, filepath):
        groups = {"ungrouped":[]}
        feeds = []

        dom = parse(filepath)
        outlines = dom.getElementsByTagName("outline")
        for outline in outlines:
            #child elements without @type are groups
            if not outline.getAttribute("type"):
                title = outline.getAttribute("title")
                if title not in groups.keys():
                    groups[title] = []
            else:
                title = outline.getAttribute("title")
                feedtype = outline.getAttribute("type")
                xmlUrl = outline.getAttribute("xmlUrl")
                htmlUrl = outline.getAttribute("htmlUrl")
                key = hashlib.sha1(xmlUrl).hexdigest()
                #create feed
                feeds.append( { "title":title,
                                "feedtype":feedtype,
                                "xmlUrl":xmlUrl,
                                "htmlUrl":htmlUrl,
                                "key":key
                                })
                #add to group
                if outline.parentNode.tagName == "outline":
                    group_title = outline.parentNode.getAttribute("title")
                    if group_title not in groups.keys():
                        groups[group_title] = [key]
                    else:
                        groups[group_title].append(key)
                else:
                    groups["ungrouped"].append(key)

        return { "imported":datetime.datetime.utcnow(),
                 "groups":groups,
                 "feeds":feeds
                 }

class FeedEater(object):

    def __init__(self):
        self.eat_every = datetime.timedelta(hours=os.environ.get("EAT_FEEDS_EVERY", 1))
    
    def eat(self, feed):
        retval = None
        check_date = datetime.datetime.utcnow()
        if (check_date - feed["last_checked"]) > self.eat_every:
            #TODO: deal with other types of feeds
            parsed_data = self.load_feed(feed)
            retval = self.cook_feed(feed, parsed_data)
            feed["last_checked"] = check_date
        return retval

    def load_feed(self,feed):
        parsed_data = feedparser.parse(feed["xmlUrl"],
                                       etag=feed.get("etag",None),
                                       modified=feed.get("modified",None)
                                       )
        #deal with status codes

        if not parsed_data.get("status",None) or parsed_data.status in [404,410]:
            feed["ignore"]=True
        elif parsed_data.status in [301]:
            feed["xmlUrl"] = parsed_data.href
        feed["etag"] = parsed_data.get("etag",None)
        feed["modified"] = parsed_data.get("modified",None)
        return parsed_data

    def cook_feed(self, feed, parsed_data):
        retval = []
        #make them good for saving 
        for parsed_entry in parsed_data.entries:
            #throw away old stories
            #get the right date from possible attributes
            entry_datetime = created = published = updated = datetime.datetime.utcnow()
            found_entry_datetime = False
            #these dates should be in UTC
            if ('published_parsed' in parsed_entry and parsed_entry.published_parsed):
                entry_datetime = published = datetime.datetime(*parsed_entry.published_parsed[:7])
                found_entry_datetime = True

            if ('created_parsed' in parsed_entry and parsed_entry.created_parsed):
                created = datetime.datetime(*parsed_entry.created_parsed[:7])
                if not found_entry_datetime:
                    found_entry_datetime = True
                    entry_datetime = created
            else:
                created = entry_datetime

            if ('updated_parsed' in parsed_entry and parsed_entry.updated_parsed):
                updated = datetime.datetime(*parsed_entry.updated_parsed[:7])
                if not found_entry_datetime:
                    found_entry_datetime = True
                    entry_datetime = updated
            else:
                updated = entry_datetime
            
            #TODO: actually look at the content types etc.
            entry = {
                "title":parsed_entry.get('title',''),
                "summary":parsed_entry.get('summary',''),
                "published":published,
                "content":parsed_entry.get('content',''),
                "publisher":parsed_entry.get('publisher','unknown'),
                "author":parsed_entry.get('author','unknown'),
                "comments":parsed_entry.get('comments',None),
                #"contributors":parsed_entry.get('contributors',''),
                "created": created ,
                #"enclosures":parsed_entry.get('enclosures',''),
                "entry_id":parsed_entry.get('id',''),
                "link":parsed_entry.get('link',None),
                #"links":parsed_entry.get('links',''),
                "updated":updated,
                "feed_key":feed["key"],
                "feed_title":feed["title"],
                "entry_datetime":entry_datetime
                }
            retval.append(entry)
        return retval


        

        


                
                                            

                              


                
        
        

