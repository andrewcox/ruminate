import os
import hashlib
import uuid
import pymongo
from datetime import datetime
from bson.objectid import ObjectId

def hash_password(salt, password):
    return hashlib.sha512(password + salt).hexdigest()

class DataAccessException(Exception):
    pass

class DataAccess(object):

    def __init__(self, db_name='ruminate'):
        mongo_uri = os.environ.get("MONGO_URI", None)
        if mongo_uri:
            self.uri = mongo_uri
            self.db_name = os.path.split(mongo_uri)[1]
        else:
            self.uri = "localhost"
            self.db_name = db_name

    def check_user_password(self, username, password):
        retval = False
        uobj = None
        with pymongo.Connection(self.uri) as c:
            db = c[self.db_name]
            col = db["user"]
            uobj = col.find_one({"username":username})
        
        if uobj and uobj.get("salt") and uobj.get("hash"):
            hashed_password = hash_password(uobj["salt"], password)
            if hashed_password == uobj["hash"]:
                retval = True

        return retval

    def add_user(self, username, password):
        salt = uuid.uuid4().hex
        uobj = {"username":username, 
                "salt": salt,
                "hash": hash_password(salt,password)
                }
        with pymongo.Connection(self.uri) as c:
            db = c[self.db_name]
            col = db["user"]
            #check for existing user
            if col.find_one({"username":username}):
                raise DataAccessException("User Exists Already")
            else:
                col.insert(uobj)

        return uobj

    def add_feeds(self, feeds):
        for feed in feeds:
            self.add_feed(feed)

    def add_feed(self, feed):
        with pymongo.Connection(self.uri) as c:
            db = c[self.db_name]
            col = db["feed"]
            col.ensure_index("key")
            if not col.find_one({"key":feed["key"]}):
                feed["inserted_on"]=datetime.utcnow()
                feed["last_checked"]=datetime.min
                col.insert(feed)
                feed["add_status"]=True
            else:
                feed["add_status"]=False
                
    def add_groups(self, username, groups):
        for groupname,feedkeys in groups.items():
            self.add_group(username,groupname,feedkeys)

    def add_group(self,username,groupname,feedkeys):
        with pymongo.Connection(self.uri) as c:
            db = c[self.db_name]
            col = db["user"]
            col.ensure_index("username")
            user = col.find_one({"username":username})
            if not user:
                raise DataAccessException("User does not exist")
            else:
                group = None
                if not user.get("groups"):
                    col.update({"_id":user["_id"]},
                               {"$set":{'groups':[]}}
                               )
                    user["groups"]=[]
                #now check if group already exists
                for existing_group in user["groups"]:
                    if groupname == existing_group["name"]:
                        group = existing_group
                        break
                
                if not group:
                    group = {"name":groupname,
                         "priority":1,
                         "inserted_on":datetime.utcnow(),
                         "feeds":feedkeys
                         }
                    col.update({"_id":user["_id"]},
                               {"$push":{'groups':group}}
                               )
                else:
                    #want to add the missing feedkeys to the end of the list
                    feeds_to_add = list(set(feedkeys)-set(group["feeds"]))

                    col.update({"_id":user["_id"], "groups.name":groupname},
                               {"$pushAll":{"groups":feeds_to_add}}
                               )
                               

    def get_user(self, username, add_feed_data=False):
        with pymongo.Connection(self.uri) as c:
            db = c[self.db_name]
            user_col = db["user"]
            feed_col = db["feed"]
            entry_col = db["entry"]
            user = user_col.find_one({"username":username})
            if not add_feed_data:
                return user

            for group in user["groups"]:
                group["feed_data"] = {}
                for feed_key in group["feeds"]:
                    #TODO: this should be a cut down obj
                    feed = feed_col.find_one({"key":feed_key})
                    group["feed_data"][feed_key] = feed

            return user

    def get_feeds(self, limit=0, sort=None):
        with pymongo.Connection(self.uri) as c:
            db = c[self.db_name]
            feed_col = db["feed"]            
            return list(feed_col.find({"ignore":{"$ne":True}},limit=limit,sort=sort))

    def add_entries(self, entries):
        for entry in entries:
            self.add_entry(entry)

    def add_entry(self, entry):
        with pymongo.Connection(self.uri) as c:
            db = c[self.db_name]
            col = db["entry"]
            col.ensure_index("feed_key")
            if not col.find_one(
                {"feed_key":entry["feed_key"],"entry_id":entry["entry_id"]}):
                entry["inserted_on"]=datetime.utcnow()
                col.insert(entry)
                entry["add_status"]=True
            else:
                entry["add_status"]=False    

    def save_feed(self,feed):
        with pymongo.Connection(self.uri) as c:
            db = c[self.db_name]
            col = db["feed"]
            col.save(feed)

    def set_last_updated(self, feed, state):
        with pymongo.Connection(self.uri) as c:
            db = c[self.db_name]
            col = db["feed"]
            col.update({"key":feed["key"]},
                       {"$set": {"last_updated":datetime.utcnow(),
                                 "last_updated_state":state}}
                       )

    def get_entries_for_group(self,group, user_id):
        retval = []
        with pymongo.Connection(self.uri) as c:
            db = c[self.db_name]
            col= db["entry"]
            retval = list(col.find(
                {"feed_key":{"$in":group["feeds"]},
                 "read_by":{"$nin":[ObjectId(user_id)]}
                 }
                ).sort("entry_datetime",pymongo.ASCENDING).limit(10))
#            print retval
        return retval

    def mark_entries_as_read(self,user,entry_ids):
        with pymongo.Connection(self.uri) as c:
            db = c[self.db_name]
            col= db["entry"]
            for entry_id in entry_ids:
                col.update({"_id":ObjectId(entry_id)},
                            {"$push":{"read_by":user["_id"]}}
                           )
     
    def update_user_groups(self,user,delete_changes,order_changes):

        #copy delete_changes into deleted_groups
        if not user.get("deleted_groups", None):
            user["deleted_groups"] = []

        for delete_change in delete_changes:
            g = user["groups"][delete_change]
            user["deleted_groups"].append(g)
            #mark for later pop as we need to preserve index
            user["groups"][delete_change] = {} 
        
        #reorder 
        order_changes.sort(key=lambda x:x[1])
        for order_change in order_changes:
            g = user["groups"].pop(order_change[0])
            user["groups"].insert(order_change[1],g)

        #pop deletes
        user["groups"] = [group for group in user["groups"] if group]
        
        #save changes
        with pymongo.Connection(self.uri) as c:
            db = c[self.db_name]
            col= db["user"]
            col.update({"_id":user["_id"]},
                       {"$set": 
                        {"groups":user["groups"],
                         "deleted_groups":user["deleted_groups"]} 
                        }
                       ) 
        
        



