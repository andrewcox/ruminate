import os
from flask import Flask
app = Flask(__name__)
app.secret_key = os.environ.get('SECRET_KEY', os.urandom(24))

import ruminate.web.views
