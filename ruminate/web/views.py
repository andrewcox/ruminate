from ruminate.web import app
from ruminate.data.access import DataAccess
from functools import wraps
from flask import request, redirect, url_for, session, flash, render_template, send_from_directory
from forms import LoginForm
import os

def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        
        if session.get('username', None) is None:
            #TODO: if token then check 
            return redirect(url_for('login'))
        return f(*args, **kwargs)
    return decorated_function

@app.context_processor
def inject_template_values():
    username = session.get('username', None)
    return dict(username=username)

@app.route('/')
@login_required
def index():
    da = DataAccess()
    user = da.get_user(session.get('username'), add_feed_data=True)

    return render_template('index.html', user=user)

@app.route('/r/<int:group_index>', methods=['GET','POST'])
@login_required
def read_group_entries(group_index):
    da = DataAccess()
    user = da.get_user(session.get('username'), add_feed_data=True)
    if request.method == 'POST':
        entries_to_work_on = [x.replace('E_','') for x in request.form.keys() if x.startswith('E_')]
        if entries_to_work_on:
            da.mark_entries_as_read(user,entries_to_work_on)
        else:
            return redirect(url_for('read_group_entries', group_index=(group_index+1)%len(user["groups"])))
        

    entries = []
    if len(user["groups"]) > int(group_index):
        #TODO: Care about read / unread entries
        entries = da.get_entries_for_group(user["groups"][int(group_index)],user["_id"])

    return render_template('read_group_entries.html', group_index=group_index ,group=user["groups"][group_index], entries=entries)

@app.route('/login/', methods=['GET', 'POST'])
def login():
    form = LoginForm(request.form)
    if request.method == 'POST' and form.validate():
        da = DataAccess()
        if not da.check_user_password(form.username.data,form.password.data):
            flash('LOGIN FAILED!!!!')
            #should do the other things here that are stops against evil
        else:
            #add cookie and redirect
            session['username'] = form.username.data  #TODO: should I be a token?
            #TODO: if there is a next then redirect there otherwise goto index
            return redirect(url_for('index'))
    return render_template('login.html', form=form)

@app.route('/logout/', methods=['GET', 'POST'])
def logout():
    session['username'] = ""
    session.pop('username', None)
    return redirect(url_for('login'))

@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'favicon.ico', mimetype='image/vnd.microsoft.icon')

@app.route('/apple-touch-icon-precomposed.png')
def apple_touch_icon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'touch-icon57.png')

@app.route('/m', methods=['GET','POST'])
@login_required
def management():
    da = DataAccess()
    user = da.get_user(session.get('username'))
    if request.method == 'POST':
        #get the order changes (1 indexed)
        print request.form.keys()
        order_changes = [ (int(key)-1,int(item)-1)
            for key,item in [
                (key.replace('O_',''),item) 
                for key,item in request.form.items() 
                if key.startswith('O_')]
            if key != item ]

        print request.form.keys()
        #get deletes (1 indexed)
        delete_changes = [ int(key.replace('D_',''))-1
                for key in request.form.keys() 
                if key.startswith('D_')]

        print delete_changes
        print order_changes
        da.update_user_groups(user,delete_changes,order_changes)

    return render_template('management.html',user=user)

@app.route('/m/<int:group_index>', methods=['GET','POST'])
@login_required
def management_group(group_index):
    da = DataAccess()
    user = da.get_user(session.get('username'), add_feed_data=True)
    return render_template('management_group.html',user=user,group_index=group_index)
