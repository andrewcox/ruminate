from flask.ext.wtf import Form, TextField, Required, PasswordField

class LoginForm(Form):
    username = TextField('Email Address',
                         validators=[Required()])
    password = PasswordField('Password',  
                             validators=[Required()])
