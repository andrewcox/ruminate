# Ruminate

A RSS feed consumer and web app to read the feed.  Just your average Google Reader replacement.

## Why

Well Google Reader is dead (long live Google Reader)

And it is a good excuse to for a (sort of) complicated project.

## Current State

Look at the [Trello board](https://trello.com/b/ZqZWaSlr)

## Environment vars

+ `MONGO_URI` = The URI to the Mongo DB to use, defaults to mongodb://localhost/ruminate
+ `SECRET_KEY` = The Flask Secret key to use, defaults to a new urandom on each start
+ `EAT_FEEDS_EVERY` = How often the feeds should be checked, defaults to 1
+ `RUMINATE_DEBUG` = True if the web app should be in debug state, defaults to False
+ `PORT` = The port to bind to, defaults to 5000

## Logo Colors
+ wheat - #f1da6d
+ red - #c51b1b 

## Heroku Setup

+ `git clone https://andrewcox@bitbucket.org/andrewcox/ruminate.git`
+ `heroku apps:create ruminate`
+ `git remote add heroku git@heroku.com:ruminate.git`
+ `git push heroku master`
+ `heroku addons:add mongolab:starter`
+ `heroku addons:add scheduler:standard`
+ `heroku config:add SECRET_KEY=<some_key>`
+ `heroku config:add MONGO_URI=<mongo lab uri>`
+ If you have a ruminate db already set up then you can dump / restore the DB
+ Otherwise `heroku run python runcommand.py add_user <username> <password>`
+ Set up `python runcommand.py load_feeds` as a scheduled task


